import { ThemeProvider } from 'theme-ui';
import theme from '../theme';
import GlobalStyles from '../theme/global-styles';

function MyApp({ Component, pageProps }) {
  return (
      <ThemeProvider theme={theme}>
          <GlobalStyles/>
        <Component {...pageProps} />
      </ThemeProvider>
  )
}

export default MyApp
