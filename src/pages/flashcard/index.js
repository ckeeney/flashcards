import Layout from "../../components/layout";

const FlashcardIndexPage = () => {
    return (
        <Layout>
            This is the flashcard index page.
        </Layout>
    )
}

export default FlashcardIndexPage;
