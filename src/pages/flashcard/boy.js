import Layout from "../../components/layout";
import {useRouter} from "next/router";
import {path} from "ramda";
import Flashcard from "../../components/flashcard";



const FlashcardPage = () => {
    const router = useRouter();
    const word = path(['query', 'word'])(router)
    return (
        <Layout>
            <Flashcard word={word}/>
        </Layout>
    )
}

export default FlashcardPage;
