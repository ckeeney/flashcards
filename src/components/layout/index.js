import { Global, css } from '@emotion/react';
const GlobalStyles = () => (
    <Global
        styles={[
            (theme) => ({
                '#__next': {
                    height: '100vh',
                    main: {
                        flex: 1,
                    },
                    display: 'flex',
                    flexDirection: 'column',
                },
            }),
        ]}
    />
);

const Layout = ({children}) => {
    return (
        <>
            <GlobalStyles/>
            <nav sx={{p: 3}}>navbar</nav>
            <main>{children}</main>
            <footer sx={{p: 3}}>footer</footer>
        </>
    )
}

export default Layout;
