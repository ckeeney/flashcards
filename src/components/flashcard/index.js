import {useEffect, useState} from "react";
import {map} from "ramda";
import {Howl, Howler} from 'howler';
// const renderWordParts = (wordParts) => {
//     return map(wordPart => <span>{wordPart}</span>)(wordParts);
// }
const Flashcard = ({word}) => {
    const wordParts = ['b', 'oy'];
    const [activeWordPart, setActiveWordPart] = useState();
    const sound = new Howl({
        src: ['https://ckeeney.gitlab.io/flashcards/boy.mp3'],
        sprite: {
            b: [0, 250],
            oy: [250, 1000],
        }
    });

    useEffect(() => {

        sound.play(activeWordPart);
    }, [activeWordPart])
    const renderWordParts = map(wordPart => <span onMouseOver={() => setActiveWordPart(wordPart)}>{wordPart}</span>);
    return (
        <div sx={{
            border: '4px solid #ccc',
            borderRadius: '16px',
            display: 'inline-flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            m: 4,
            px: '128px',
            h1: {
                fontSize: '128px',
                my: 0,
                'span:hover': {
                    backgroundColor: 'palegreen',
                    cursor: 'pointer',
                }
            }
        }}>
            <h1>{renderWordParts(wordParts)}</h1>
            <audio src={`https://ckeeney.gitlab.io/flashcards/boy.mp3`} controls/>
            <div>active word part: {activeWordPart}</div>
        </div>
    );
}

export default Flashcard;
