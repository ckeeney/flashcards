// these values override the default theme at https://github.com/system-ui/theme-ui/blob/stable/packages/preset-base/src/index.js
import colors from './colors';
export default {
    colors,

    // 35 rem = 35 rem * 16 px/rem = 560px.
    // 60 rem = 60 rem * 16 px/rem = 960px.
    // 91 rem = 91 rem * 16 px/rem = 1456px.
    breakpoints: ['35rem', '60rem', '91rem'], // for some reason this is computing at 1rem = 24px?
    // breakpoints: ['560px', '960px', '1399px'],
    // breakpoints: ['560px', '960px', '1455px'],
    fonts: {
        body: 'Inter, sans-serif',
    },
    fontSizes: [13, 14, 16, 18, 24, 32, 40, 56, 72],
    space: [0, 4, 8, 12, 16, 24, 32, 64, 128, 256, 512],
    fontWeights: {
        light: 300,
        body: 400,
        bold: 600,
        heading: 700,
    },
    lineHeights: {
        body: 1.5,
        // "reading" is what I'm calling the serif body text styles, so use that lineHeight token accordingly
        bodyReading: 1.8,
        subtitle: 1.6,
        heading: 1.125,
        largeHeading: 1.5,
    },
    letterSpacings: {
        bodySmall: 0.4,
    },
    shadows: {
        small: '0px 4px 8px rgba(137, 137, 137, 0.2)',
        medium: '0px 12px 32px rgba(137, 137, 137, 0.24)',
        large: '0px 24px 40px rgba(137, 137, 137, 0.16)',
    },
    radii: {
        none: 0,
        default: '0.25rem',
        full: '9999px',
    },
    styles: {
        root: {
            // uses the theme values provided above
            fontFamily: 'body',
            fontSize: 1,
        },
    },
};
