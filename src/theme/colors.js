
const primary = [
    '#0000ff',
    '#6666ff',
    '#aaaaff',
];

const grayscale = [
    '#333',
    '#6c6c6c',
    '#898989',
    '#c4c4c4',
    '#d3d3d3',
    '#e1e1e1',
    '#f0f0f0',
    '#fff',
];
grayscale.dark = grayscale[0];
grayscale.white = grayscale[grayscale.length - 1]; // last item of grayscale array

export default {
    // we may want these to be refactored into the styles object below, and using the token variables as references. rather than manually specifying here
    text: grayscale.dark,
    textSecondary: grayscale[1],
    textContrast: grayscale.white,
    textContrastSecondary: 'rgba(255,255,255, 0.72)',
    textContrastDisabled: 'rgba(255,255,255,0.4)',
    background: grayscale.white,
    muted: '#e0e0e0',
    highlight: '#9f9f9f',
    gray: grayscale[1],
    accent: '#3f3f3f',
    primary,
    grayscale,
};
